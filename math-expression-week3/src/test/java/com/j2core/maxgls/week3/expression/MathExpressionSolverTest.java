package com.j2core.maxgls.week3.expression;

import com.j2core.maxgls.week3.expression.parser.ExpressionRegexParser;
import org.junit.Assert;
import org.junit.Test;

/**
 * Unit tests for {@link MathExpressionSolver}
 *
 * @author maxgls
 */
public class MathExpressionSolverTest {

    private final MathExpressionSolver solver = new MathExpressionSolver(new ExpressionRegexParser());

    @Test
    public void operatorPriorityWithoutBracket() throws MathExpressionValidationException {
        this.assertCalcResult("3/4*5+6-7/2-1*4/1.5", 3.58);
    }

    @Test
    public void operatorPriorityWithBracket() throws MathExpressionValidationException {
        this.assertCalcResult("(2+2)+(100+(2*3+(3*3+2)*2)*2+10)", 170);
    }

    @Test
    public void onlyNegativeNumber() throws MathExpressionValidationException {
        this.assertCalcResult("-123", -123);
    }

    @Test
    public void minusBeforeOpeningBracket() throws MathExpressionValidationException {
        this.assertCalcResult("-(-(2))", 2);
    }

    @Test
    public void onlyOneOperator() throws MathExpressionValidationException {
        this.assertErrorMessageContains("-", "Expression can not start with operator [-]");
    }

    @Test
    public void onlyOneClosingBracket() throws MathExpressionValidationException {
        this.assertErrorMessageContains(")", "The count of opening brackets not equal count of of closing bracket");
    }

    @Test
    public void noValueInBrackets() throws MathExpressionValidationException {
        this.assertErrorMessageContains("()", "The sequence of opening bracket and closing bracket is not allowed");
    }

    @Test
    public void noOperatorBetweenBrackets() throws MathExpressionValidationException {
        this.assertErrorMessageContains("(1)(2)", "The sequence of closing bracket and opening bracket is not allowed.");
    }

    @Test
    public void closingBracketBeforeOpening() throws MathExpressionValidationException {
        this.assertErrorMessageContains("1+2)+2*3+(", "Expression has unclosed [1] brackets");
    }

    @Test
    public void noClosingBracket() throws MathExpressionValidationException {
        this.assertErrorMessageContains("1+2+(2*3+", "Expression has unclosed [1] brackets");
    }

    @Test
    public void operatorBeforeClosingBracket() throws MathExpressionValidationException {
        this.assertErrorMessageContains("(2+2+)", "The sequence of operator [+] and closing bracket is not allowed");
    }

    @Test
    public void operandBeforeOpeningBracket() throws MathExpressionValidationException {
        this.assertErrorMessageContains("4(2+1)", "The sequence of operand [4] and opening bracket is not allowed");
    }

    @Test
    public void operandAfterClosingBracket() throws MathExpressionValidationException {
        this.assertErrorMessageContains("(2+1)4", "The sequence of operand [4] and closing bracket is not allowed");
    }

    @Test
    public void notAllowedOperatorBetweenBrackets() throws MathExpressionValidationException {
        this.assertErrorMessageContains("(*(2+2))", "The sequence of opening bracket and operator [*] is not allowed");
    }

    @Test
    public void twoOperators() throws MathExpressionValidationException {
        this.assertErrorMessageContains("2++2", "The sequence of two following operators [+,+] is not allowed.");
    }

    private void assertCalcResult(String expression, double expectedResult) throws MathExpressionValidationException {
        final Double result = this.solver.calculate(expression);
        Assert.assertEquals(expectedResult, result, 2);
    }

    private void assertErrorMessageContains(String expression, String expectedMessage) {
        String actualErrorMessage = "";
        try {
            this.solver.calculate(expression);
        } catch (MathExpressionValidationException ex) {
            actualErrorMessage = ex.getMessage();
        }
        Assert.assertTrue(actualErrorMessage, actualErrorMessage.contains(expectedMessage));
    }
}