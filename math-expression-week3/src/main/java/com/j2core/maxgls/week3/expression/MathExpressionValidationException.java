package com.j2core.maxgls.week3.expression;

/**
 * Validation errors in mathematical expression
 *
 * @author maxgls
 */
public class MathExpressionValidationException extends Exception {

    public MathExpressionValidationException(String validationError) {
        super(validationError);
    }
}