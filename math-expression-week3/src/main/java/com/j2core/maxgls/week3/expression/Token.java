package com.j2core.maxgls.week3.expression;

/**
 * Represents token of mathematical expression
 *
 * @author maxgls
 */
public class Token {

    private final String value;
    private final TokenType type;

    private Token(String value, TokenType type) {
        this.value = value;
        this.type = type;
    }

    public static Token create(String value) {
        if (value == null || value.isEmpty()) {
            throw new IllegalArgumentException("The parameter 'value' is not specified");
        }
        final TokenType type = TokenType.fromTokenValue(value);
        if (TokenType.OPERAND == type) {
            // Fail fast. Validate operand value
            validateOperandValue(value);
        }
        return new Token(value, type);
    }

    public String getValue() {
        return this.value;
    }

    public TokenType getType() {
        return this.type;
    }

    private static boolean isDouble(String tokenValue) {
        try {
            Double.parseDouble(tokenValue);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    private static void validateOperandValue(String tokenValue) {
        if (!isDouble(tokenValue)) {
            throw new IllegalArgumentException(String.format("OPERAND '%s' is not number", tokenValue));
        }
    }
}