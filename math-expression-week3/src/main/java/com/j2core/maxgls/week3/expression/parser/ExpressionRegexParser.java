package com.j2core.maxgls.week3.expression.parser;

import com.j2core.maxgls.week3.expression.Token;
import com.j2core.maxgls.week3.expression.TokenOperator;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Regex parser for mathematical expression.
 *
 * Supported tokens:
 *      Operands: positive and negative numbers
 *      Operators: + - * /
 *      Brackets: ( )
 *
 * @author maxgls
 */
public class ExpressionRegexParser implements ExpressionParser {

    private static final String REGEX_NUMBER = "(?<!\\d)[-]?\\d*\\.?\\d+";
    private static final String REGEX_OPERATOR = "([\\+\\-\\*/])";
    private static final String REGEX_BRACKET = "([\\(\\)])";
    private static final String REGEX_OTHER = "(.)";

    /**
     * Parses mathematical expression using regex
     *
     * @param expression mathematical expression
     * @return list of infix tokens
     */
    @Override
    public List<Token> parse(String expression) {
        if (expression == null) {
            throw  new IllegalArgumentException("The parameter 'expression' is null");
        }
        final String preparedExpression = this.replaceUnaryNegationOperators(expression);
        final Pattern pattern = Pattern.compile(REGEX_NUMBER + "|" +
                REGEX_OPERATOR + "|" +
                REGEX_BRACKET + "|" +
                REGEX_OTHER);
        final Matcher matcher = pattern.matcher(preparedExpression);

        final List<Token> result = new ArrayList<>();
        while(matcher.find()) {
            final String tokenValue = matcher.group();
            result.add(Token.create(tokenValue));
        }

        return result;
    }

    /**
     * Replace unary negation operators to normal format:
     *  - start with '-(', replace to '-1*('
     *  - replace '(-(' to '(-1*('
     *
     *
     * @param expression mathematical expression
     * @return mathematical expression with replaced negative opening brackets
     */
    private String replaceUnaryNegationOperators(String expression) {
        final String negativeOpeningBracketStart =
                TokenOperator.SUB.toString() +
                        TokenOperator.OPENING_BRACKET;
        final String negativeOpeningBracketStartStartNormal =
                TokenOperator.SUB.toString() +
                        "1" +
                        TokenOperator.MULTI +
                        TokenOperator.OPENING_BRACKET;

        String preparedExpression = expression;
        if (preparedExpression.startsWith(negativeOpeningBracketStart)) {
            // Replace expression Start with '-(' to '-1*('
            preparedExpression =
                    negativeOpeningBracketStartStartNormal +
                            preparedExpression.substring(2);
        }

        // Replace '(-(' to '(-1*('
        return preparedExpression.replace(
                TokenOperator.OPENING_BRACKET + negativeOpeningBracketStart,
                TokenOperator.OPENING_BRACKET + negativeOpeningBracketStartStartNormal);
    }
}