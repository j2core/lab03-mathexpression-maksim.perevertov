package com.j2core.maxgls.week3.expression;

import com.j2core.maxgls.week3.expression.converter.infix2postfix.InfixToPostfixConverter;
import com.j2core.maxgls.week3.expression.parser.ExpressionParser;
import com.j2core.maxgls.week3.expression.validate.MathExpressionValidator;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;
import java.util.Stack;

/**
 * Facade for calculating mathematical expression
 *
 * @author maxgls
 */
public class MathExpressionSolver {

    private static final MathExpressionValidator validator = new MathExpressionValidator();
    private final ExpressionParser parser;

    public MathExpressionSolver(ExpressionParser parser) {
        this.parser = parser;
    }

    /**
     * Calculate result of the mathematical expression
     *
     * @param expression the value of mathematical expression
     * @return result of calculation
     * @throws MathExpressionValidationException
     */
    public double calculate(String expression) throws MathExpressionValidationException {
        // Parse mathematical expression to infix tokens
        final List<Token> infixTokens = this.parser.parse(expression);

        // Validate token sequence of mathematical expression
        final List<String> errors = validator.validateTokens(infixTokens);
        if (!errors.isEmpty()){
            throw new MathExpressionValidationException(getValidationErrorMessage(errors));
        }

        // Convert infix tokens to postfix one
        final List<Token> postfixTokens = InfixToPostfixConverter.convert(infixTokens);

        // Calculate result of mathematical expression in postfix format
        return calculate(postfixTokens);
    }

    private static String getValidationErrorMessage(final List<String> errors) {
        final StringBuilder sb = new StringBuilder();
        for (String error : errors){
            sb.append(error).append("\r\n");
        }
        return sb.toString();
    }

    /**
     * Calculate result of mathematical expression in postfix format
     *
     * @param postfixTokens mathematical expression in postfix format
     * @return result of calculation
     */
    private static double calculate(final List<Token> postfixTokens) {
        final Stack<Double> operandStack = new Stack<>();
        for (Token token: postfixTokens) {
            final TokenType tokenType = token.getType();
            final String tokenValue = token.getValue();
            switch (tokenType) {
                case OPERAND:
                    operandStack.push(Double.parseDouble(tokenValue));
                    break;
                case OPERATOR:
                    operandStack.push(calculateOperands(tokenValue, operandStack));
                    break;
                default:
                    throw new IllegalArgumentException(String.format("The token type:'%s' is not allowed in postfix form", tokenType));
            }
        }

        // Get result of calculation from stack
        return operandStack.pop();
    }

    /**
     * POP two operands from stack and calculate result regarding operator
     *
     * @param operator the value of calculation operator
     * @param operandStack stack with operands
     * @return result of calculation of two operands
     */
    private static double calculateOperands(final String operator, final Stack<Double> operandStack) {
        if (operandStack.size() < 2) {
            throw new IllegalArgumentException(String.format("Not enough operands in stack for operation:'%s'", operator));
        }
        final double secondOperand = operandStack.pop();
        final double firstOperand = operandStack.pop();

        // Perform operation between first and second operand.
        final TokenOperator tokenOperator = TokenOperator.fromString(operator);
        return calculateNumbers(tokenOperator, firstOperand, secondOperand);
    }

    private static double calculateNumbers(final TokenOperator operator, double firstOperand, double secondOperand) {
        switch (operator) {
            case SUM:
                return firstOperand + secondOperand;
            case SUB:
                return firstOperand - secondOperand;
            case MULTI:
                return firstOperand * secondOperand;
            case DIV:
                return firstOperand / secondOperand;
            default:
                throw new NotImplementedException();
        }
    }
}