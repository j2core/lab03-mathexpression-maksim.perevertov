package com.j2core.maxgls.week3.expression.validate;

import com.j2core.maxgls.week3.expression.Token;
import com.j2core.maxgls.week3.expression.TokenType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Performs validation for mathematical expression tokens
 *
 * @author maxgls
 */
public class MathExpressionValidator {

    public List<String> validateTokens(final List<Token> infixTokens) {
        final List<String> errors = new ArrayList<>();
        final IntHolder tokenCounter =  new IntHolder(0);
        for (int i = 0; i < infixTokens.size(); i++) {
            final Token previousToken = i - 1 >= 0 ? infixTokens.get(i - 1) : null;
            final Token nextToken = i + 1 < infixTokens.size() ? infixTokens.get(i + 1) : null;
            final Token currentToken = infixTokens.get(i);
            final List<String> validationResult = validateToken(currentToken, previousToken, nextToken, tokenCounter);
            if (!validationResult.isEmpty()) {
                validationResult.add(0,"Token index: " + i);
            }
            errors.addAll(validationResult);
        }
        return errors;
    }

    private List<String> validateToken(final Token token, final Token previousToken, final Token nextToken, final IntHolder counter) {
        final List<String> errors = new ArrayList<>();
        errors.addAll(validateOperatorToken(token, previousToken, nextToken));
        errors.addAll(validateOperandToken(token, previousToken, nextToken));
        errors.addAll(validateBracketToken(token, previousToken, nextToken));
        errors.addAll(validateCountBrackets(token, nextToken, counter));

        return errors;
    }

    // Validation algorithms for different token types (Interface and implementations have been deleted to make this simpler):
    private static class IntHolder {
        private int value;

        public IntHolder(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public void increment() {
            this.value++;
        }

        public void decrement() {
            this.value--;
        }
    }

    /**
     * Validate 'Operator' token against specific rules:
     *      - R1: The next token is not null (last token cannot be operator)
     *      - R2: previous token is not null (first token cannot be operator)
     *      - R3: The next token is not Operator
     *      - R4: previous token is not '('
     *      - R5: The next token is not ')'
     *
     * @param token current infix token to validate
     * @param previousToken token before current token
     * @param nextToken token after current token
     * @return The error collection. There are no errors if list is empty.
     */
    private static Collection<String> validateOperatorToken(final Token token, final Token previousToken, final Token nextToken) {
        final List<String> errors = new ArrayList<>();
        if (token.getType() != TokenType.OPERATOR) {
            return errors;
        }
        final String tokenValue = token.getValue();
        // R1: The next token is not null (last token cannot be operator)
        if (nextToken == null && previousToken != null) {
            errors.add(String.format("Expression can not be end on operator [%s].", tokenValue));
        }
        // R2: previous token is not null (first token cannot be operator)
        if (previousToken == null) {
            errors.add(String.format("Expression can not start with operator [%s].", tokenValue));
        }
        // R3: The next token is not Operator
        if (nextToken != null && nextToken.getType() == TokenType.OPERATOR) {
            errors.add(String.format("The sequence of two following operators [%1$s,%2$s] is not allowed.", tokenValue, nextToken.getValue()));
        }
        // R4: previous token is not '('
        if (previousToken != null && previousToken.getType() == TokenType.OPENING_BRACKET) {
            errors.add(String.format("The sequence of opening bracket and operator [%s] is not allowed.", tokenValue));
        }
        // R5: The next token is not ')'
        if (nextToken != null && nextToken.getType() == TokenType.CLOSING_BRACKET) {
            errors.add(String.format("The sequence of operator [%s] and closing bracket is not allowed.", tokenValue));
        }
        return errors;
    }

    /**
     * Validate 'Operand' token against specific rules:
     *      - R1: The next token is not Operand
     *      - R2: The next token is not '('
     *      - R3: previous token is not ')'
     *
     * @param token current infix token to validate
     * @param previousToken token before current token
     * @param nextToken token after current token
     * @return The error collection. There are no errors if list is empty.
     */
    private static Collection<String> validateOperandToken(final Token token, final Token previousToken, final Token nextToken) {
        final List<String> errors = new ArrayList<>();
        if (token.getType() != TokenType.OPERAND) {
            return errors;
        }
        final String tokenValue = token.getValue();
        // R1: The next token is not Operand
        if (nextToken != null && nextToken.getType() == TokenType.OPERAND) {
            errors.add(String.format("The sequence of two following operands [%1$s,%2$s] is not allowed.", tokenValue, nextToken.getValue()));
        }
        // R2: The next token is not '('
        if (nextToken != null && nextToken.getType() == TokenType.OPENING_BRACKET) {
            errors.add(String.format("The sequence of operand [%s] and opening bracket is not allowed.", tokenValue));
        }
        // R3: Previous token is not ')'
        if (previousToken != null && previousToken.getType() == TokenType.CLOSING_BRACKET) {
            errors.add(String.format("The sequence of operand [%s] and closing bracket is not allowed.", tokenValue));
        }
        return errors;
    }

    /**
     * Validate Bracket token against specific rules:
     *      - R1: token '(' and next token is not ')'
     *      - R2: token '(' and previous token is not ')'
     *
     * @param token current infix token to validate
     * @param previousToken token before current token
     * @param nextToken token after current token
     * @return The error collection. There are no errors if list is empty.
     */
    private static Collection<String> validateBracketToken(final Token token, final Token previousToken, final Token nextToken) {
        final List<String> errors = new ArrayList<>();
        if (token.getType() != TokenType.OPENING_BRACKET) {
            return errors;
        }
        // R1: token '(' and next token is not ')'
        if (nextToken != null && nextToken.getType() == TokenType.CLOSING_BRACKET) {
            errors.add("The sequence of opening bracket and closing bracket is not allowed.");
        }
        // R2: token '(' and previous token is not ')'
        if (previousToken != null && previousToken.getType() == TokenType.CLOSING_BRACKET) {
            errors.add("The sequence of closing bracket and opening bracket is not allowed.");
        }
        return errors;
    }

    /**
     * Validate count opening and closing bracket tokens against specific rules:
     *      - R1: count of opening brackets == count of closing brackets
     *
     * @param token         current infix token to validate
     * @param nextToken     token after current token
     * @param counter token counter is used as buffer for validation stuff
     * @return The error collection. There are no errors if list is empty.
     */
    private static Collection<String> validateCountBrackets(final Token token, final Token nextToken, final IntHolder counter) {
        final List<String> errors = new ArrayList<>();
        if (token.getType() == TokenType.OPENING_BRACKET) {
            counter.increment();
        }
        if (token.getType() == TokenType.CLOSING_BRACKET) {
            counter.decrement();
            if (counter.getValue() < 0) {
                errors.add("The count of opening brackets not equal count of of closing bracket.");
                counter.setValue(0);
            }
        }
        if (nextToken == null && counter.getValue() > 0) {
            errors.add(String.format("Expression has unclosed [%d] brackets.", counter.getValue()));
        }
        return errors;
    }
}