package com.j2core.maxgls.week3.expression;

/**
 * Represents toke type of mathematical expression
 *
 * @author maxgls
 */
public enum TokenType {

    OPERAND,
    OPERATOR,
    OPENING_BRACKET,
    CLOSING_BRACKET;

    /**
     * Gets token type by token value of mathematical expression
     *
     * @param tokenValue Token value of mathematical expression
     * @return Token type of mathematical expression
     */
    public static TokenType fromTokenValue(String tokenValue) {
        final TokenOperator tokenOperator = TokenOperator.fromString(tokenValue);
        if (tokenOperator == null) {
            return TokenType.OPERAND;
        }
        switch (tokenOperator) {
            case CLOSING_BRACKET:
                return TokenType.CLOSING_BRACKET;
            case OPENING_BRACKET:
                return TokenType.OPENING_BRACKET;
            default:
                return TokenType.OPERATOR;
        }
    }
}