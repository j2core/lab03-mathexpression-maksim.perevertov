package com.j2core.maxgls.week3.expression.converter.infix2postfix;

import com.j2core.maxgls.week3.expression.Token;
import com.j2core.maxgls.week3.expression.TokenOperator;
import com.j2core.maxgls.week3.expression.TokenType;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Convert math expression from infix format to postfix one
 *
 * @author maxgls
 */
public final class InfixToPostfixConverter {

    private InfixToPostfixConverter() {
    }

    /**
     * Convert list of math expression tokens from infix format to postfix one
     *
     * @param infixTokens The list of math expression tokens in infix format
     * @return The list of math expression tokens in postfix format
     */
    public static List<Token> convert(final List<Token> infixTokens) {
        // Initialize 'Context' for infix to postfix converting
        final List<Token> postfixTokens = new ArrayList<>(infixTokens.size());
        final Stack<Token> opTokensStack = new Stack<>();

        // Process the list of infix tokens
        for(Token token : infixTokens) {
            TokenType tokenType = token.getType();
            switch (tokenType){
                case OPERAND:
                    processOperand(token, postfixTokens);
                    break;
                case OPERATOR:
                    processOperator(token, postfixTokens, opTokensStack);
                    break;
                case OPENING_BRACKET:
                    processOpeningBracket(token, opTokensStack);
                    break;
                case CLOSING_BRACKET:
                    processClosingBracket(postfixTokens, opTokensStack);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        // POP the rest of operators from stack and save them to list of postfix tokens
        while (!opTokensStack.empty()){
            postfixTokens.add(opTokensStack.pop());
        }

        return postfixTokens;
    }

    // Conversion algorithms for different token types (Interface and implementations have been deleted to make this simpler):
    /**
     * Process 'Closing Bracket' token.
     *
     *  Algorithm:
     *      POP operators from stack until '(' is not reached
     *
     * @param postfixTokens list of postfix tokens
     * @param opTokensStack stack with operators
     */
    private static void processClosingBracket(final List<Token> postfixTokens, final Stack<Token> opTokensStack) {
        while (!opTokensStack.empty()) {
            final Token opToken = opTokensStack.pop();
            if (TokenOperator.OPENING_BRACKET.toString().equals(opToken.getValue())) {
                break;
            }
            postfixTokens.add(opToken);
        }
    }

    /**
     * Process 'Operator' token.
     *
     *  Algorithm:
     *      Check whether stack contains operator with greater or equal priority:
     *          -YES: POP operators with greater or equal priority and save to list of postfix tokens
     *          -NO: PUSH token operator to stack
     *
     * @param token current 'Operator' token for processing
     * @param postfixTokens list of postfix tokens
     * @param opTokensStack stack with operators
     */
    private static void processOperator(final Token token, final List<Token> postfixTokens, final Stack<Token> opTokensStack) {
        // Save to output list if stack is empty
        if (opTokensStack.empty()) {
            opTokensStack.push(token);
            return;
        }

        // Copy stack to array for safety 'pop' and 'push' operations during iteration/reading.
        // Stack will be copied to array from down to top, so we will read array from end to start.
        final Token[] opTokens = opTokensStack.toArray(new Token[opTokensStack.size()]);
        final String operator = token.getValue();
        final int tokenOpPriority = TokenOperator.fromString(operator).getPriority();
        // POP operators from stack with greater or equal priority
        for (int i = opTokens.length - 1; i >= 0; i--){
            final Token op = opTokens[i];
            final int opPriority = TokenOperator.fromString(op.getValue()).getPriority();
            if (opPriority < tokenOpPriority) {
                break;
            }
            postfixTokens.add(opTokensStack.pop());
        }

        // PUSH token to stack
        opTokensStack.push(token);
    }

    /**
     * Process 'Operand' token.
     *
     *  Algorithm:
     *     Add token to list of postfix tokens
     *
     * @param token current 'Operand' token for processing
     * @param postfixTokens list of postfix tokens
     */
    private static void processOperand(final Token token, final List<Token> postfixTokens) {
        postfixTokens.add(token);
    }

    /**
     * Process 'OpeningBracket' token.
     *
     *  Algorithm:
     *     Add token to stack with operators
     *
     * @param token current 'OpeningBracket' token for processing
     * @param opTokensStack stack with operators
     */
    private static void processOpeningBracket(final Token token, final Stack<Token> opTokensStack){
        opTokensStack.push(token);
    }
}