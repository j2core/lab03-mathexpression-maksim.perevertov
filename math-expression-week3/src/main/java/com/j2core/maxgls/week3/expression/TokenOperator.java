package com.j2core.maxgls.week3.expression;

/**
 * Represents token operator and mathematical priority
 *
 * @author maxgls
 */
public enum TokenOperator {

    CLOSING_BRACKET(")", 0),
    OPENING_BRACKET("(", 0),
    SUM("+", 1),
    SUB("-", 1),
    MULTI("*", 2),
    DIV("/", 2);

    private final String operator;
    private final int priority;

    TokenOperator(String operator, int priority) {
        this.operator = operator;
        this.priority = priority;
    }

    public String getOperator() {
        return this.operator;
    }

    public int getPriority() {
        return this.priority;
    }

    public static TokenOperator fromString(String operator) {
        if (operator == null) {
            return null;
        }

        for (TokenOperator tokenOperator : TokenOperator.values()) {
            if (operator.equals(tokenOperator.operator)) {
                return tokenOperator;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return this.operator;
    }
}