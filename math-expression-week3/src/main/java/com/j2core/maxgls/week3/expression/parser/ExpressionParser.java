package com.j2core.maxgls.week3.expression.parser;

import com.j2core.maxgls.week3.expression.Token;

import java.util.List;

/**
 * Parser for mathematical expression
 *
 * @author maxgls
 */
public interface ExpressionParser {

    /**
     * Parses mathematical expression and returns list of tokens
     *
     * @param expression mathematical expression
     * @return list of infix tokens
     */
    List<Token> parse(String expression);
}