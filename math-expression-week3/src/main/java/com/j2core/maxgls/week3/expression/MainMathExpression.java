package com.j2core.maxgls.week3.expression;

import com.j2core.maxgls.week3.expression.parser.ExpressionRegexParser;

import java.util.Scanner;

/**
 * Entry point for calculating mathematical expression
 *
 * @author maxgls
 */
public class MainMathExpression {

    /**
     * Calculates mathematical expression from terminal.
     *
     * @param args parameter will be ignored. Console input will be used to enter mathematical expression
     */
    public static void main(String[] args) throws MathExpressionValidationException {
        System.out.println("Please enter mathematical expression:");
        final Scanner scanner = new Scanner(System.in, "UTF-8");
        String expression = scanner.nextLine();
        scanner.close();
        expression = expression.replace(" ","").replace("\t","");

        // Calculate result of mathematical expression:
        final double result = new MathExpressionSolver(new ExpressionRegexParser()).calculate(expression);
        System.out.println("Result: " + result);
    }
}